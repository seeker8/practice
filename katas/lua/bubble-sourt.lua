
function bubbleSort(a2s)
  local swapped = false
  repeat
    swapped = false
    for i = 1, (#a2s - 1) do
      if a2s[i] > a2s[i+1] then
        local temp = a2s[i]
        a2s[i] = a2s[i+1]
        a2s[i+1] = temp
        swapped = true
      end
    end
  until not swapped
  return a2s
end

--test function
local sorted = bubbleSort({1,7,3,5,4})

for i = 1, #sorted do
  print(sorted[i])
end